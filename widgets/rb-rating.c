/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 *  arch-tag: Implementation of rating renderer object
 *
 *  Copyright (C) 2002 Olivier Martin <oleevye@wanadoo.fr>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
 *
 */

#include "config.h"

#include <string.h>
#include <gtk/gtk.h>

#include "rb-rating.h"
#include "rb-rating-helper.h"
#include "rb-stock-icons.h"
#include "rb-cut-and-paste-code.h"

/* Offset at the beggining of the widget */
#define X_OFFSET 0

/* Vertical offset */
#define Y_OFFSET 2

/* Number of stars */
#define MAX_SCORE 5

#define COLOR_OFFSET 120

static void rb_rating_class_init (RBRatingClass *class);
static void rb_rating_init (RBRating *label);
static void rb_rating_finalize (GObject *object);
static void rb_rating_get_property (GObject *object,
				    guint param_id,
				    GValue *value,
				    GParamSpec *pspec);
static void rb_rating_set_property (GObject *object,
				    guint param_id,
				    const GValue *value,
				    GParamSpec *pspec);
static void rb_rating_size_request (GtkWidget *widget,
				    GtkRequisition *requisition);
static gboolean rb_rating_expose (GtkWidget *widget,
				  GdkEventExpose *event);
static gboolean rb_rating_button_press_cb (GtkWidget *widget,
					   GdkEventButton *event,
					   RBRating *rating);

struct RBRatingPrivate
{
	double rating;
	RBRatingPixbufs *pixbufs;
};

G_DEFINE_TYPE (RBRating, rb_rating, GTK_TYPE_EVENT_BOX)
#define RB_RATING_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), RB_TYPE_RATING, RBRatingPrivate))

enum
{
	PROP_0,
	PROP_RATING
};

enum
{
	RATED,
	LAST_SIGNAL
};

static guint rb_rating_signals[LAST_SIGNAL] = { 0 };

static void
rb_rating_class_init (RBRatingClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class;

	widget_class = (GtkWidgetClass*) klass;

	object_class->finalize = rb_rating_finalize;
	object_class->get_property = rb_rating_get_property;
	object_class->set_property = rb_rating_set_property;

	widget_class->expose_event = rb_rating_expose;
	widget_class->size_request = rb_rating_size_request;

	rb_rating_install_rating_property (object_class, PROP_RATING);

	rb_rating_signals[RATED] =
		g_signal_new ("rated",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (RBRatingClass, rated),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__DOUBLE,
			      G_TYPE_NONE,
			      1,
			      G_TYPE_DOUBLE);

	g_type_class_add_private (klass, sizeof (RBRatingPrivate));
}

static void
rb_rating_init (RBRating *rating)
{
	rating->priv = RB_RATING_GET_PRIVATE (rating);

	/* create the needed icons */
	rating->priv->pixbufs = rb_rating_pixbufs_new ();

	/* register some signals */
	g_signal_connect_object (G_OBJECT (rating),
				 "button_press_event",
				 G_CALLBACK (rb_rating_button_press_cb),
				 rating, 0);
}

static void
rb_rating_finalize (GObject *object)
{
	RBRating *rating;

	rating = RB_RATING (object);

	if (rating->priv->pixbufs != NULL) {
		rb_rating_pixbufs_free (rating->priv->pixbufs);
	}

	G_OBJECT_CLASS (rb_rating_parent_class)->finalize (object);
}

static void
rb_rating_get_property (GObject *object,
			guint param_id,
			GValue *value,
			GParamSpec *pspec)
{
	RBRating *rating = RB_RATING (object);

	switch (param_id) {
	case PROP_RATING:
		g_value_set_double (value, rating->priv->rating);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
rb_rating_set_property (GObject *object,
			guint param_id,
			const GValue *value,
			GParamSpec *pspec)
{
	RBRating *rating= RB_RATING (object);

	switch (param_id) {
	case PROP_RATING:
		rating->priv->rating = g_value_get_double (value);
		gtk_widget_queue_draw (GTK_WIDGET (rating));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

RBRating *
rb_rating_new ()
{
	RBRating *rating;

	rating = g_object_new (RB_TYPE_RATING, NULL, NULL);

	g_return_val_if_fail (rating->priv != NULL, NULL);

	return rating;
}

static void
rb_rating_size_request (GtkWidget *widget,
			GtkRequisition *requisition)
{
	int icon_size;

	g_return_if_fail (requisition != NULL);

	gtk_icon_size_lookup (GTK_ICON_SIZE_MENU, &icon_size, NULL);

	requisition->width = RB_RATING_MAX_SCORE * icon_size + X_OFFSET;
	requisition->height = icon_size + Y_OFFSET * 2;
}

static gboolean
rb_rating_expose (GtkWidget *widget,
		  GdkEventExpose *event)
{
	int icon_size;
	gboolean ret;

	g_return_val_if_fail (RB_IS_RATING (widget), FALSE);

	gtk_icon_size_lookup (GTK_ICON_SIZE_MENU, &icon_size, NULL);

	ret = FALSE;

	if (GTK_WIDGET_DRAWABLE (widget) == TRUE) {
		RBRating *rating = RB_RATING (widget);

		/* make the widget prettier */
		gtk_paint_flat_box (widget->style, widget->window,
				    GTK_STATE_NORMAL, GTK_SHADOW_IN,
				    NULL, widget, "entry_bg", 0, 0,
				    widget->allocation.width,
				    widget->allocation.height);

		gtk_paint_shadow (widget->style, widget->window,
				  GTK_STATE_NORMAL, GTK_SHADOW_IN,
				  NULL, widget, "text", 0, 0,
				  widget->allocation.width,
				  widget->allocation.height);

		/* draw the stars */
		if (rating->priv->pixbufs != NULL) {
			ret = rb_rating_render_stars (widget,
						      widget->window,
						      rating->priv->pixbufs,
						      0, 0,
						      X_OFFSET, Y_OFFSET,
						      rating->priv->rating,
						      FALSE);
		}
	}

	return ret;
}

static gboolean
rb_rating_button_press_cb (GtkWidget *widget,
			   GdkEventButton *event,
			   RBRating *rating)
{
	int mouse_x, mouse_y;
	double new_rating;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (RB_IS_RATING (rating), FALSE);

	gtk_widget_get_pointer (widget, &mouse_x, &mouse_y);

	new_rating = rb_rating_get_rating_from_widget (widget, mouse_x,
						       widget->allocation.width,
						       rating->priv->rating);

	if (new_rating == -1.0) {
		return FALSE;
	} else {
		g_signal_emit (G_OBJECT (rating),
			       rb_rating_signals[RATED],
			       0, new_rating);
		return TRUE;
	}
}
