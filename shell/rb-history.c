/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 *  arch-tag: Implementation of Song History List
 *
 *  Copyright (C) 2003 Jeffrey Yasskin <jyasskin@mail.utexas.edu>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
 *
 */

#include <string.h>

#include "rb-history.h"

#include "rhythmdb.h"
#include "gsequence.h"

struct RBHistoryPrivate
{
	GSequence *seq;
	/* If seq is empty, current == g_sequence_get_end_ptr (seq) */
	GSequencePtr current;

	GHashTable *entry_to_seqptr;

	gboolean truncate_on_play;
	guint maximum_size;

	GFunc destroyer;
	gpointer destroy_userdata;
};

#define RB_HISTORY_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), RB_TYPE_HISTORY, RBHistoryPrivate))

#define MAX_HISTORY_SIZE 50

static void rb_history_class_init (RBHistoryClass *klass);
static void rb_history_init (RBHistory *shell_player);
static void rb_history_finalize (GObject *object);

static void rb_history_set_property (GObject *object,
				     guint prop_id,
				     const GValue *value,
				     GParamSpec *pspec);
static void rb_history_get_property (GObject *object,
				     guint prop_id,
				     GValue *value,
				     GParamSpec *pspec);

static void rb_history_limit_size (RBHistory *hist, gboolean cut_from_beginning);

/**
 * Removes a link from the history and updates all pointers. Doesn't destroy the entry, but does change the size of the list.
 */
static void rb_history_delete_link (RBHistory *hist, GSequencePtr to_delete);

enum
{
	PROP_0,
	PROP_TRUNCATE_ON_PLAY,
	PROP_MAX_SIZE,
};

G_DEFINE_TYPE (RBHistory, rb_history, G_TYPE_OBJECT)

static void
rb_history_class_init (RBHistoryClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = rb_history_finalize;

	object_class->set_property = rb_history_set_property;
	object_class->get_property = rb_history_get_property;

	g_object_class_install_property (object_class,
					 PROP_TRUNCATE_ON_PLAY,
					 g_param_spec_boolean ("truncate-on-play",
							       "Truncate on Play",
							       "Whether rb_history_set_playing() truncates the rest of the list",
							       FALSE,
							       G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class,
					 PROP_MAX_SIZE,
					 g_param_spec_uint ("maximum-size",
							    "Maximum Size",
							    "Maximum length of the history. Infinite if 0",
							    0, G_MAXUINT,
							    0,
							    G_PARAM_READWRITE));

	g_type_class_add_private (klass, sizeof (RBHistoryPrivate));
}

RBHistory *
rb_history_new (gboolean truncate_on_play, GFunc destroyer, gpointer destroy_userdata)
{
	RBHistory *history;

	history = g_object_new (RB_TYPE_HISTORY,
				"truncate-on-play", truncate_on_play,
				NULL);

	g_return_val_if_fail (history->priv != NULL, NULL);

	history->priv->destroyer = destroyer;
	history->priv->destroy_userdata = destroy_userdata;

	return history;
}

static void
rb_history_init (RBHistory *hist)
{
	hist->priv = RB_HISTORY_GET_PRIVATE (hist);

	hist->priv->entry_to_seqptr = g_hash_table_new (g_direct_hash,
							g_direct_equal);
	hist->priv->seq = g_sequence_new (NULL);
	hist->priv->current = g_sequence_get_begin_ptr (hist->priv->seq);
}

static void
rb_history_finalize (GObject *object)
{
	RBHistory *hist;
	g_return_if_fail (object != NULL);
	g_return_if_fail (RB_IS_HISTORY (object));

	hist = RB_HISTORY (object);

	/* remove all of the stored entries */
	rb_history_clear (hist);

	g_hash_table_destroy (hist->priv->entry_to_seqptr);
	g_sequence_free (hist->priv->seq);

	G_OBJECT_CLASS (rb_history_parent_class)->finalize (object);
}

static void
rb_history_set_property (GObject *object,
			 guint prop_id,
			 const GValue *value,
			 GParamSpec *pspec)
{
	RBHistory *hist = RB_HISTORY (object);

	switch (prop_id)
	{
	case PROP_TRUNCATE_ON_PLAY: {
		hist->priv->truncate_on_play = g_value_get_boolean (value);
		} break;
	case PROP_MAX_SIZE: {
		hist->priv->maximum_size = g_value_get_uint (value);
		rb_history_limit_size (hist, TRUE);
		} break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
rb_history_get_property (GObject *object,
			 guint prop_id,
			 GValue *value,
			 GParamSpec *pspec)
{
	RBHistory *hist = RB_HISTORY (object);

	switch (prop_id)
	{
	case PROP_TRUNCATE_ON_PLAY: {
		g_value_set_boolean (value, hist->priv->truncate_on_play);
		} break;
	case PROP_MAX_SIZE: {
		g_value_set_uint (value, hist->priv->maximum_size);
		} break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}

}

RBHistory *
rb_history_clone (RBHistory *orig, GFunc callback, gpointer userdata)
{
	GSequencePtr ptr;
	RBHistory *hist = rb_history_new (orig->priv->truncate_on_play,
					  orig->priv->destroyer,
					  orig->priv->destroy_userdata);
	rb_history_set_maximum_size (hist, orig->priv->maximum_size);

	for (ptr=g_sequence_get_begin_ptr (orig->priv->seq); !g_sequence_ptr_is_end (ptr); ptr = g_sequence_ptr_next (ptr)) {
		if (callback)
			callback (g_sequence_ptr_get_data (ptr), userdata);
		rb_history_append (hist, g_sequence_ptr_get_data (ptr));

		/* Set current to point to the same place in the history */
		if (orig->priv->current == ptr) {
			rb_history_go_last (hist);
		}
	}

	return hist;
}

void
rb_history_set_destroy_notify (RBHistory *hist, GFunc destroyer, gpointer destroy_userdata)
{
	g_return_if_fail (RB_IS_HISTORY (hist));

	hist->priv->destroyer = destroyer;
	hist->priv->destroy_userdata = destroy_userdata;
}

void
rb_history_set_truncate_on_play (RBHistory *hist, gboolean truncate_on_play)
{
	g_return_if_fail (RB_IS_HISTORY (hist));

	hist->priv->truncate_on_play = truncate_on_play;
	g_object_notify (G_OBJECT (hist), "truncate-on-play");
}

void
rb_history_set_maximum_size (RBHistory *hist, guint maximum_size)
{
	g_return_if_fail (RB_IS_HISTORY (hist));

	hist->priv->maximum_size = maximum_size;
	g_object_notify (G_OBJECT (hist), "maximum-size");
}

guint
rb_history_length (RBHistory *hist)
{
	g_return_val_if_fail (RB_IS_HISTORY (hist), 0);

	return g_sequence_get_length (hist->priv->seq);
}

RhythmDBEntry *
rb_history_first (RBHistory *hist)
{
	GSequencePtr begin;
	g_return_val_if_fail (RB_IS_HISTORY (hist), NULL);

	begin = g_sequence_get_begin_ptr (hist->priv->seq);
	return g_sequence_ptr_is_end (begin) ? NULL : g_sequence_ptr_get_data (begin);
}

RhythmDBEntry *
rb_history_previous (RBHistory *hist)
{
	GSequencePtr prev;

	g_return_val_if_fail (RB_IS_HISTORY (hist), NULL);

	prev = g_sequence_ptr_prev (hist->priv->current);
	return prev == hist->priv->current ? NULL : g_sequence_ptr_get_data (prev);
}

RhythmDBEntry *
rb_history_current (RBHistory *hist)
{
	g_return_val_if_fail (RB_IS_HISTORY (hist), NULL);

	return g_sequence_ptr_is_end (hist->priv->current) ? NULL : g_sequence_ptr_get_data (hist->priv->current);
}

RhythmDBEntry *
rb_history_next (RBHistory *hist)
{
	GSequencePtr next;
	g_return_val_if_fail (RB_IS_HISTORY (hist), NULL);

	next = g_sequence_ptr_next (hist->priv->current);
	return g_sequence_ptr_is_end (next) ? NULL : g_sequence_ptr_get_data (next);
}

RhythmDBEntry *
rb_history_last (RBHistory *hist)
{
	GSequencePtr last;

	g_return_val_if_fail (RB_IS_HISTORY (hist), NULL);

	last = g_sequence_ptr_prev (g_sequence_get_end_ptr (hist->priv->seq));
	return g_sequence_ptr_is_end (last) ? NULL : g_sequence_ptr_get_data (last);
}

void
rb_history_go_first (RBHistory *hist)
{
	g_return_if_fail (RB_IS_HISTORY (hist));

	hist->priv->current = g_sequence_get_begin_ptr (hist->priv->seq);
}

void
rb_history_go_previous (RBHistory *hist)
{
	GSequencePtr prev;
	g_return_if_fail (RB_IS_HISTORY (hist));

	prev = g_sequence_ptr_prev (hist->priv->current);
	if (prev)
		hist->priv->current = prev;
}

void
rb_history_go_next (RBHistory *hist)
{
	g_return_if_fail (RB_IS_HISTORY (hist));

	hist->priv->current = g_sequence_ptr_next (hist->priv->current);
}

void
rb_history_go_last (RBHistory *hist)
{
	GSequencePtr last;
	g_return_if_fail (RB_IS_HISTORY (hist));

	last = g_sequence_ptr_prev (g_sequence_get_end_ptr (hist->priv->seq));
	hist->priv->current = last ? last : g_sequence_get_end_ptr (hist->priv->seq);
}

void
rb_history_set_playing (RBHistory *hist, RhythmDBEntry *entry)
{
	g_return_if_fail (RB_IS_HISTORY (hist));

	if (entry == NULL) {
		hist->priv->current = g_sequence_get_end_ptr (hist->priv->seq);
		return;
	}

	rb_history_remove_entry (hist, entry);

	g_sequence_insert (g_sequence_ptr_next (hist->priv->current), entry);
	/* make hist->priv->current point to the new entry */
	if (g_sequence_ptr_is_end (hist->priv->current))
		hist->priv->current = g_sequence_ptr_prev (hist->priv->current);
	else
		hist->priv->current = g_sequence_ptr_next (hist->priv->current);
	g_hash_table_insert (hist->priv->entry_to_seqptr, entry, hist->priv->current);

	if (hist->priv->truncate_on_play) {
		while (!g_sequence_ptr_is_end (g_sequence_ptr_next (hist->priv->current)))
			rb_history_remove_entry (hist, g_sequence_ptr_get_data (g_sequence_ptr_next (hist->priv->current)));
	}

	rb_history_limit_size (hist, TRUE);
}

void
rb_history_append (RBHistory *hist, RhythmDBEntry *entry)
{
	GSequencePtr new_node, last;

	g_return_if_fail (RB_IS_HISTORY (hist));
	g_return_if_fail (entry != NULL);

	if (entry == g_sequence_ptr_get_data(hist->priv->current)) {
		rb_history_remove_entry (hist, entry);
		last = g_sequence_ptr_prev (g_sequence_get_end_ptr (hist->priv->seq));
		hist->priv->current = last ? last : g_sequence_get_end_ptr (hist->priv->seq);
	} else {
		rb_history_remove_entry (hist, entry);
	}

	g_sequence_append (hist->priv->seq, entry);
	new_node = g_sequence_ptr_prev (g_sequence_get_end_ptr (hist->priv->seq));
	g_hash_table_insert (hist->priv->entry_to_seqptr, entry, new_node);

	rb_history_limit_size (hist, TRUE);
}

gint
rb_history_get_current_index (RBHistory *hist)
{
	g_return_val_if_fail (RB_IS_HISTORY (hist), -1);

	return g_sequence_ptr_get_position (hist->priv->current);
}

void
rb_history_insert_at_index (RBHistory *hist, RhythmDBEntry *entry, guint index)
{
	GSequencePtr old_node;
	GSequencePtr new_node;

	g_return_if_fail (RB_IS_HISTORY (hist));
	g_return_if_fail (entry != NULL);
	g_return_if_fail (index <= g_sequence_get_length (hist->priv->seq));

	/* Deal with case where the entry is moving forward */
	old_node = g_hash_table_lookup (hist->priv->entry_to_seqptr, entry);
	if (old_node && g_sequence_ptr_get_position (old_node) < index)
		index--;

	rb_history_remove_entry (hist, entry);

	new_node = g_sequence_get_ptr_at_pos (hist->priv->seq, index);
	g_sequence_insert (new_node, entry);
	new_node = g_sequence_ptr_prev (new_node);
	g_hash_table_insert (hist->priv->entry_to_seqptr, entry, new_node);

	if (g_sequence_ptr_is_end (hist->priv->current) && index == g_sequence_get_length (hist->priv->seq)-1 /*length just increased*/)
		hist->priv->current = new_node;

	rb_history_limit_size (hist, TRUE);
}

/**
 * Cuts nodes off of the history from the desired end until it is smaller than max_size.
 * Never cuts off the current node.
 */
static void
rb_history_limit_size (RBHistory *hist, gboolean cut_from_beginning)
{
	if (hist->priv->maximum_size != 0)
		while (g_sequence_get_length (hist->priv->seq) > hist->priv->maximum_size) {
			if (cut_from_beginning
					|| hist->priv->current == g_sequence_ptr_prev (g_sequence_get_end_ptr (hist->priv->seq))) {
				rb_history_remove_entry (hist, rb_history_first (hist));
			} else {
				rb_history_remove_entry (hist, rb_history_last (hist));
			}
		}
}

void
rb_history_remove_entry (RBHistory *hist, RhythmDBEntry *entry)
{
	GSequencePtr to_delete;
	g_return_if_fail (RB_IS_HISTORY (hist));

	to_delete = g_hash_table_lookup (hist->priv->entry_to_seqptr, entry);
	if (to_delete) {
		g_hash_table_remove (hist->priv->entry_to_seqptr, entry);
		if (hist->priv->destroyer)
			hist->priv->destroyer (entry, hist->priv->destroy_userdata);
		rb_history_delete_link (hist, to_delete);
	}
}

static void
rb_history_delete_link (RBHistory *hist, GSequencePtr to_delete)
{
	if (to_delete == hist->priv->current) {
		hist->priv->current = g_sequence_get_end_ptr (hist->priv->seq);
	}
	g_assert (to_delete != hist->priv->current);

	g_sequence_remove (to_delete);
}

void
rb_history_clear (RBHistory *hist)
{
	g_return_if_fail (RB_IS_HISTORY (hist));

	while (!g_sequence_ptr_is_end (g_sequence_get_begin_ptr (hist->priv->seq)))
		rb_history_remove_entry (hist, rb_history_first (hist));

	/* When the sequence is empty, the hash table should also be empty. */
	g_assert (g_hash_table_size (hist->priv->entry_to_seqptr) == 0);
}

GPtrArray *
rb_history_dump (RBHistory *hist)
{
	GSequencePtr cur;
	GPtrArray *result;

	g_return_val_if_fail (RB_IS_HISTORY (hist), NULL);

	result = g_ptr_array_sized_new (g_sequence_get_length (hist->priv->seq));
	for (cur = g_sequence_get_begin_ptr (hist->priv->seq);
	     !g_sequence_ptr_is_end (cur);
	     cur = g_sequence_ptr_next (cur)) {
		g_ptr_array_add (result, g_sequence_ptr_get_data (cur));
	}
	return result;
}

gboolean
rb_history_contains_entry (RBHistory *hist, RhythmDBEntry *entry)
{
	g_return_val_if_fail (RB_IS_HISTORY (hist), FALSE);

	return g_hash_table_lookup (hist->priv->entry_to_seqptr, entry) != NULL;
}
